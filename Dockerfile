FROM golang:1.6

# Change name and entrypoint
ARG name=backend
ARG build=debug
ARG version=0.1.0
ARG time=0

COPY backend/ /go/src/bitbucket.org/bytewise-team/backend
RUN export bkname=$name && mkdir -p /go/src/bitbucket.org/bytewise-team/backend && \
    cd /go/src/bitbucket.org/bytewise-team/backend && \
    go get -d -v && \
    go install -v -ldflags \
        "-X main.NAME=$name\
        -X main.VERSION=$version\
        -X main.BUILD=$build\
        -X main.TIME=$time"\
    $package

CMD /go/bin/$bkname

EXPOSE 80

# Bytewise I/O Webapp

## TODO
1. Find go web framework
3. Scaffold out webapp
4. Begin API design


## Submodules
Due to the fact that this repo uses submodules cloning and pulling is a bit more involved than usual.

#### Clone
Make sure to add the `--recursive` flag when cloning the repo
```sh
git clone https://bitbucket.org/bytewise-team/webapp.git --recursive

git submodule update --recursive
```

#### Pull
To update the submodules use this command
```sh
git submodule foreach git pull origin master
```
#### Push
Only work on one repo at a time. Always push submodules before this repo.


## Development
The build system is simple and very scalable. It relies on `make` and `docker` to build an image and run the server in a default environment
The frontend is under development so there is no `make client` option right now;
#### server
```sh
make server
```
#### Run
```sh
make run   # go to localhost:8080
```
#### Build and Run
```sh
make   #localhost:8080 for now
```


#### Build Parameters
When using make, you can change version or type of your current build directly from the command line. This makes the [semantic versioning](http://semver.org/)  that is in place easy to implement.
The parameters passed to make will filter down into the server itself at compilation so it can be used at runtime.

##### version
The version follows [semantic versioning](http://semver.org/) and should be changed in the makefile itself when new version branches are made

##### build (debug, test, release)
The build is the build type or mode that is being used. Debug, test and release can be used.
```sh
make version=2.1.3 build=release
```

#### Speeding up builds
Docker is a great container platform to use but without perfect optimization build times can be less than desirable. To speed that up you can use a simple `make` flag.
```sh
make -j8			# Uses 8 cores
make -j$(nproc)		# Uses all system cores  (nproc == 8)
```

## License (MIT)
See the [LICENSE](https://bitbucket.org/bytewise-team/webapp/src/1bb8ecf78dfcb3c19918d78d2438a0e9720957f8/LICENSE) file for complete licensing information

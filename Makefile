###
# The makefile for the Bytewise backend. See the LICENSE file for more.
###

# Constants
MAIN=main.go
DOCKER=docker
BACKEND=backend
FRONTEND=frontend
DOCKERFILER=Dockerfile

BUILD_DEBUG=debug
BUILD_RELEASE=release
BUILD_TEST=test

BUILD_TIME=$(shell date +%FT%T%z)

# Args
name  	?= $(notdir $(shell pwd))
version ?= 0.1.0
build  	?= $(BUILD_DEBUG)

# Flags for building the server
sflags :=	--build-arg version=${version} \
					--build-arg build=${build}

#Flags for running docker
rflags := -it -p 8080:80 --rm

SERVER := server
RUN 	 := run



.PHONY: all
all: $(SERVER) $(RUN)

.PHONY: $(SERVER)
$(SERVER): $(DOCKERFILE)
	@$(DOCKER) build $(sflags) -t $(name) .

.PHONY: $(RUN)
$(RUN): $(SERVER)
	@$(DOCKER) run  $(rflags) --name bwdev_ins_0 $(name)
